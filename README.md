# Concurrent Programming C++

This repository contains my homework assignments for University course Concurrent Programming with C++

Topics covered in assignments include :

* multithreading
* mutexes
* deadlocks
* conditional variables
* future / promise
* atomics
* async operations

# Assignments info

**bank_account.cpp**

* Create a class that simulates a bank account operations and holds the current balance of the account.
* Add member functions for deposit, withdraw and balance.
* Declare two bank account objects of aforementioned class and initialize with some balance.
* Start 4 threads that will make random deposit and withdraw transactions between these two accounts.
* Protect the bank account data with mutexes and avoid deadlocks.

**conditional_variables.cpp**

* Create a worker thread with a long running task.
* Use 5 condition variables within the worker thread to notify the main thread of the progress.
* Each condition variable amounts to 20% of the total task done
* In main thread, wait for the condition variables to complete and print the progress of the worker thread.

**bank_account_atomic.cpp** 

* Take the code from home assignment 4 (bank_account.cpp) to modify it for this assignment
* Add function addInterest to bank account class.
* After 100 deposit or withdraw operations, an addInterest function is called: addInterest adds 0.05 per cent interest to account balance (based on current balance).
* Modify the app to operate lock-free using atomics.

**future_promise.cpp**

* Declare a class Sensor.
* Sensor uses a thread internally to read sensor values.
* Simulate sensor values by generating random numbers in between 0-4095
* Declare a class SensorReader.
* SensorReader allocates 4 sensors and starts a thread to read sensor values.
* SensorReader thread can be stopped by a function call to SensorReader::stop.
* Assume situation that reader is only interested of sensor values larger than 4000.
* Implement future-promise; reader will wait for the result of sensor value over 4000.
* When SensorReader gets a sensor value over 4000, print it to the console.
* On your main function, allocate a SensorReader and let it run until user presses a key in console.

**async_operations.cpp**

* Declare a class 'RandomNumberGenerator'. In class, specify enum of task type as LIGHT or HEAVY.
* If RandomNumberGenerator is LIGHT, it will generate 100 random numbers.
* If RandomNumberGenerator is HEAVY, it will generate 10 000 000 random numbers.
* Allocate 100 RandomNumberGenerators and set the task type to LIGHT or HEAVY randomly.
* Run the allocated RandomNumberGenerators as async tasks.
* If generator is LIGHT, use deferred launching.
* If generator is HEAVY, use async launching.
* Measure how long it takes to complete all generators.
* Modify your code to run all tasks with async launching.
* Measure how long it takes to complete all generators.
* Add comments into the source code of your findings. Which way is faster? Why?

**picture_exercise.cpp**

* There was a base code given which did not run in parallel. This code worked through 3 large pictures pixel by pixel.
* Assignment was to make this code concurrent and improve performance.
* Base code ran on my computer 5.5 seconds.
* I added parallellism so that work was split between 10 threads and managed to reduce performance time to 1.0 seconds.
