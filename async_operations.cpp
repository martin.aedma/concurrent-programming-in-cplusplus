#include <iostream>
#include <chrono>
#include <future>
#include <random>
#include <vector>
#include <chrono>

using namespace std;
using namespace std::chrono;

class RandomNumberGenerator
{
public:

	enum Task { LIGHT, HEAVY };

	RandomNumberGenerator(bool task)
	{
		if (task)
		{
			m_Task = LIGHT;
			m_iCapacity = 100;
		} else
		{
			m_Task = HEAVY;
			m_iCapacity = 1000000;
		}
	}

	int generateNumber()
	{
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<> distr(0, 10000000);

		return distr(gen);
	}

	int fillNumbers()
	{
		for (size_t i = 0; i<m_iCapacity; i++)
		{
			m_vNumbers.emplace_back(generateNumber());
		}

		return 0;
	}
	
	Task m_Task;
	vector<int> m_vNumbers;
	int m_iCapacity;
};

int main()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> distr(1, 100);

	vector<RandomNumberGenerator> generators;
	generators.reserve(100);

	for (size_t i = 0; i< 100; i++)
	{
		if(distr(gen) % 2 == 0)
		{
			generators.emplace_back(RandomNumberGenerator(true));
		} else
		{
			generators.emplace_back(RandomNumberGenerator(false));
		}		
	}	

	int test = 0;

	// collect futures to vector
	vector<future<int>> futures;

	// stopwatch
	high_resolution_clock stopWatch;

	cout << "Starting clock : ... " << endl;

	// START CLOCK
	auto start = stopWatch.now();
	
	for (size_t i = 0; i < 100; i++)
	{	
		if(generators[i].m_Task == RandomNumberGenerator::LIGHT)
		{
			future<int> number = async(launch::deferred, &RandomNumberGenerator::fillNumbers, &generators[i]);
			futures.push_back(move(number));
		}else
		{
			future<int> number = async(launch::async, &RandomNumberGenerator::fillNumbers, &generators[i]);
			futures.push_back(move(number));
		}		
	}

	for (size_t i = 0; i<100; i++)
	{
		int a = futures[i].get();
	}
	
	// STOP CLOCK
	auto stop = stopWatch.now();

	chrono::duration<double> elapsed = stop - start;

	cout << "Time : " << elapsed.count() << endl;

	// I noticed a bit faster times with deferred & async.
	// With deferred & async I got around ~26-27 sec.
	// With async & async I got around ~28-29 sec.
	// It also depends on how many LIGHT vs HEAVY generators there are. As it's random there are differences in each execution.
	// Deferred runs on calling thread, async will always create new thread
	
}
