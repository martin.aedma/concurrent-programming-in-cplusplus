// NOTICE! I have changed language standard to use as C++17 under project settings. This allows to use scoped_lock

#include <iostream>
#include <thread>
#include <mutex>
#include <functional>
#include <random>

using namespace std;

class Account
{
public:
	Account(string _name)
	{
		name = _name;
		m_iBalance = 1000;
	}

	int checkBalance(mutex& m) const
	{
		scoped_lock<mutex> sl(m);
		return m_iBalance;
	}

	void transfer(mutex& m1, mutex& m2, Account& acc, int amount, string& thread)
	{
		if (m_iBalance - amount < 0)
		{
			cout << thread << " low funds, needed " << amount << " balance : " << m_iBalance << endl;
			return;
		}
		
		{ // lock scope
			// Im using scoped_lock here which is available starting from C++ 17. I had to change project settings and
			// set language standard to C++ 17 to be able to use it.
			// this seems to be able to lock arbitrary number of mutexes all at once while internally using deadlock-avoidance
			// algorithm, making it easy and simple to use.
			scoped_lock<mutex, mutex> sl(m1, m2);
			withdraw(amount);
			acc.deposit(amount);
		}		

		cout <<  thread << " " << amount << " from " << name << " to " << acc.name << endl;
	}

	void deposit(int amount)
	{
		m_iBalance += amount;
	}

	void withdraw(int amount)
	{
		m_iBalance -= amount;
	}

	string name;

private:
	int m_iBalance;
};


size_t randomNumber(size_t min, size_t max)
{
	mt19937 rng;
	rng.seed(random_device()());
	uniform_int_distribution<mt19937::result_type> dist(min, max);
	return dist(rng);
}

void executeOperation(mutex& m1, mutex& m2, Account& acc1, Account& acc2, string& thread)
{
	const auto operation = randomNumber(1, 2);

	switch (operation)
	{
	case 1:
		acc1.transfer(m1, m2, acc2, randomNumber(0, 500), thread);
		break;
	case 2:
		acc2.transfer(m1, m2, acc1, randomNumber(0, 500), thread);
		break;
	default:
		break;
	}
}

void doMultipleOperations(int number, mutex& m1, mutex& m2, Account& acc1, Account& acc2, string& thread)
{
	for (auto i = 0; i < number; i++)
	{
		executeOperation(m1, m2, acc1, acc2, thread);
	}

	cout << "Thread : " << thread << " finished transactions" << endl;
}


int main()
{
	Account acc1("1");
	Account acc2("2");
	mutex m1, m2;

	// Number of transaction to be executed by each thread
	auto number = 100;

	string one = "t1";
	string two = "t2";
	string three = "t3";
	string four = "t4";

	cout << "Starting balance \nAccount 1 : " << acc1.checkBalance(m1) << "\nAccount 2 : " << acc2.checkBalance(m2) << endl;


	thread t1(doMultipleOperations, ref(number), ref(m1), ref(m2), ref(acc1), ref(acc2), ref(one));
	thread t2(doMultipleOperations, ref(number), ref(m1), ref(m2), ref(acc1), ref(acc2), ref(two));
	thread t3(doMultipleOperations, ref(number), ref(m1), ref(m2), ref(acc1), ref(acc2), ref(three));
	thread t4(doMultipleOperations, ref(number), ref(m1), ref(m2), ref(acc1), ref(acc2), ref(four));

	t1.join();
	t2.join();
	t3.join();
	t4.join();

	cout << "Final balance \nAccount 1 : " << acc1.checkBalance(m1) << "\nAccount 2 : " << acc2.checkBalance(m2) << endl;

	return 0;
}
