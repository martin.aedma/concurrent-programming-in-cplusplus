#include <iostream>
#include <thread>
#include <random>
#include <atomic>

using namespace std;

class Account
{
public:
	Account(string _name)
	{
		name = _name;
		m_iBalance = 1000.f;
		m_iCounter = 0;
		m_iInterest = 0;
	}

	float checkBalance() const
	{
		return m_iBalance;
	}

	void transfer(Account& acc, int amount, string& thread)
	{		
		const int newCounter = ++m_iCounter;
		
		if (newCounter % 100 == 0)
		{
			const float balanceNow = m_iBalance.load();
			addInterest(thread, balanceNow);
		}
		
		withdraw(acc, amount);
		
	}

	void deposit(int amount)
	{
		float old = m_iBalance.load();
		while(!m_iBalance.compare_exchange_weak(old, old + amount))
		{
			//
		}
	}

	void withdraw(Account& acc, int amount)
	{
		float oldAmount, newAmount;
		// Make sure account does not go negative
		do
		{
			oldAmount = m_iBalance.load();
			newAmount = oldAmount - amount;
			if (newAmount < 0) return;
		} while (!m_iBalance.compare_exchange_weak(oldAmount, newAmount));

		// only deposit to second account if withdraw succeeds on current account
		acc.deposit(amount);
		
	}

	void addInterest(string& thread, float balance)
	{
		const float interest = 0.05f;
		const float amount = balance * interest;		
		
		while(!m_iBalance.compare_exchange_weak(balance, balance+amount))
		{
			//
		}
		++m_iInterest;	
	}

	int checkCounter() const
	{
		return m_iCounter;
	}

	int checkInterest() const
	{
		return m_iInterest;
	}

	string name;

private:
	atomic<float> m_iBalance;
	atomic<int> m_iCounter;
	atomic<int> m_iInterest;
	atomic<bool> m_bFlag;
};

size_t randomNumber(size_t min, size_t max)
{
	mt19937 rng;
	rng.seed(random_device()());
	uniform_int_distribution<mt19937::result_type> dist(min, max);
	return dist(rng);
}

void executeOperation(Account& acc1, Account& acc2, string& thread)
{
	const auto operation = randomNumber(1, 2);
	switch (operation)
	{
	case 1:
		acc1.transfer(acc2, randomNumber(0, 500), thread);
		break;
	case 2:
		acc2.transfer(acc1, randomNumber(0, 500), thread);
		break;
	default:
		break;
	}
}

void doMultipleOperations(int number,Account& acc1, Account& acc2, string& thread)
{
	for (auto i = 0; i < number; i++)
	{
		executeOperation(acc1, acc2, thread);
	}
}

int main()
{
	Account acc1("1");
	Account acc2("2");
	
	// Number of transaction to be executed by each thread
	auto number = 1500;

	string one = "t1";
	string two = "t2";
	string three = "t3";
	string four = "t4";

	cout << "Starting balance \nAccount 1 : " << acc1.checkBalance() << "\nAccount 2 : " << acc2.checkBalance() << endl;

	thread t1(doMultipleOperations, ref(number), ref(acc1), ref(acc2), ref(one));
	thread t2(doMultipleOperations, ref(number), ref(acc1), ref(acc2), ref(two));
	thread t3(doMultipleOperations, ref(number), ref(acc1), ref(acc2), ref(three));
	thread t4(doMultipleOperations, ref(number), ref(acc1), ref(acc2), ref(four));

	t1.join();
	t2.join();
	t3.join();
	t4.join();

	cout << "Final balance \nAccount 1 : " << acc1.checkBalance() << "\nAccount 2 : " << acc2.checkBalance() << endl;
	cout << "Transactions done -> Acc1 : " << acc1.checkCounter() << " Acc2 : " << acc2.checkCounter() << endl;
	cout << "Each thread performed " << number << " transactions" << endl;
	cout << "Interest added Acc1 x times : " << acc1.checkInterest() << endl;
	cout << "Interest added Acc2 x times : " << acc2.checkInterest() << endl;

	return 0;
}
