#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>

using namespace std;

class Program
{
public:
	Program()
	{
		m_progress = 0;
		m_check = false;
	}

	void loopWork()
	{
		cout << "Working ... : " << endl;
		
		work(m_cv1);
		work(m_cv2);
		work(m_cv3);
		work(m_cv4);
		work(m_cv5);

		cout << "\nFinished!" << endl;
	}

	void work(condition_variable& cv)
	{
		
		
		{	// lock scope
			unique_lock<mutex> mlock(m_mutex);
			cv.wait(mlock, [this] { return m_check; });
			m_progress++;
			
			// adding pause to simulate task / work		
			this_thread::sleep_for(chrono::milliseconds(1000));
			
			cout << " ... " << m_progress * 20 << "%" << endl;
			m_check = false;
		}
		// notify that m_check is false again
		cv.notify_one();		
	}

	void checkPoint(condition_variable& cv)
	{
				
		{	// lock scope
			lock_guard<mutex> guard(m_mutex);
			cout << " < Checking >" << endl;
			m_check = true;
		}
		
		cv.notify_one();

		// I had a synchronization problem here and eventually I got it to work by using this logic that
		// when m_check is false, thread 1 waits for thread 2 to make it true and notify thread 1-> THEN
		// thread 2 starts to wait for thread 1 to finish work. When thread 1 finishes work it will make m_check false again.
		// thread 2 is waiting until m_check is false and gets notified.
		// So both threads alternate tasks and wait for signal which is m_check -> true or false
		
		{	// lock scope
			unique_lock<mutex> mlock(m_mutex);
			cv.wait(mlock, [this] {return not m_check; });
		}
		
	}

	void loopCheckPoints()
	{
		checkPoint(m_cv1);
		checkPoint(m_cv2);
		checkPoint(m_cv3);
		checkPoint(m_cv4);
		checkPoint(m_cv5);
	}

	
private:
	mutex						m_mutex;
	condition_variable			m_cv1, m_cv2, m_cv3, m_cv4, m_cv5;
	int							m_progress;
	bool						m_check;
};

int main()
{
	Program program;
	
	thread t1(&Program::loopWork, &program);
	thread t2(&Program::loopCheckPoints, &program);

	t1.join();
	t2.join();
	
    return 0;
}
