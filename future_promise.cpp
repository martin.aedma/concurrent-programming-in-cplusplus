// NB! I changed random number range to 0 - 100000 , and reporting if number > 99990. With original numbers range each thread
// found a number > 4000 so fast that they always reported in order thread1, thread2, thread3, thread4.
// With increased range some threads find number before other threads and now it's visible in output

#include <iostream>
#include <future>
#include <random>
#include <thread>
#include <conio.h>

using namespace std;

class Sensor
{
public:

	void generateNumber(promise<int>& num)
	{
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<> distr(0, 100000);

		int x = distr(gen);

		while (x < 99990)
		{
			x = distr(gen);
		}

		num.set_value(x);
	}

};

class SensorReader
{
public:

	string startGenerating()
	{
		while(!_kbhit())
		{
			promise<int> num1, num2, num3, num4;
			shared_future<int> f1, f2, f3, f4;

			f1 = num1.get_future();
			f2 = num2.get_future();
			f3 = num3.get_future();
			f4 = num4.get_future();		
			
			Sensor sensor;
			
			thread t1(&Sensor::generateNumber, &sensor, ref(num1));
			thread t2(&Sensor::generateNumber, &sensor, ref(num2));
			thread t3(&Sensor::generateNumber, &sensor, ref(num3));
			thread t4(&Sensor::generateNumber, &sensor, ref(num4));

			if (f1._Is_ready()) 
			{
				cout << "1: " << f1._Get_value() << endl;
			}

			if (f2._Is_ready())
			{
				cout << "2: " << f2._Get_value() << endl;
			}

			if (f3._Is_ready())
			{
				cout << "3: " << f3._Get_value() << endl;
			}

			if (f4._Is_ready())
			{
				cout << "4: " << f4._Get_value() << endl;
			}
						
			t1.join();
			t2.join();
			t3.join();
			t4.join();
		}

		return "Finished!";
	}	
};

int main()
{
	SensorReader numbers;
	cout << numbers.startGenerating() << endl;
}
