/*
 * Basic program ran for 5.5 seconds
 * 
 * I divided for loops in both functions between 10 threads. Each thread receives 1/10th of for loop (4096*4096/10 = 1677721), but it does
 * not divide perfect, there is 0.6 float (1677721.6) so first 9 threads take each 1677721 sized portion of for loop and final thread
 * takes 1677721+6 (0.6*10). Each thread reads and writes to a different part of array, no synchronization needed there.
 *
 * First, image one is created with 10 threads, then second and finally third.
 *
 * Execution time reduced to 1.0 seconds. Improved in speed over 80%!
 */


#include <iostream>
#include <chrono>
#include <thread>
#include <vector>

using namespace std;

struct Pixel
{
    float red;
    float green;
    float blue;
};


void addPixelColors(const Pixel* image1, const Pixel* image2, Pixel* result, int forSize, int threadNum)
{
    int i, range;
    if (threadNum != 9)
    {
        i = threadNum * forSize;
        range = i + forSize;
    }
    else
    {
        i = threadNum * (forSize - 6);
        range = i + forSize;
    }
	
    for (int j = i; j < range; j++)
    {
        result[j].red = image1[j].red + image2[j].red;
        if (result[j].red > 1.0f)
        {
            result[j].red = 1.0f;
        }

        result[j].green = image1[j].green + image2[j].green;
        if (result[j].green > 1.0f)
        {
            result[j].green = 1.0f;
        }

        result[j].blue = image1[j].blue + image2[j].blue;
        if (result[j].blue > 1.0f)
        {
            result[j].blue = 1.0f;
        }
    }
}


void createPixels(Pixel* img, int forSize, int threadNum)
{
    int i, range;
	if(threadNum != 9)
	{
        i = threadNum * forSize;
        range = i + forSize;
	} else
	{
        i = threadNum * (forSize - 6);
        range = i + forSize;
	}
    
	
    for ( int j = i ; j < range; j++)
    {
        img[j].red = (float(rand()) / float((RAND_MAX)));
        img[j].green = (float(rand()) / float((RAND_MAX)));
        img[j].blue = (float(rand()) / float((RAND_MAX)));
    }
 
}


int main()
{
    chrono::steady_clock::time_point begin = chrono::steady_clock::now();

    constexpr int imageSize = 4096 * 4096;

    const int tenthSize = 1677721;
    const int final = 1677721 + 6;

    Pixel* first = new Pixel[imageSize];
    Pixel* second = new Pixel[imageSize];
    Pixel* combined = new Pixel[imageSize];

    vector<thread> threads;


	// Create first image with 10 threads

	for (size_t i = 0; i < 10; i++)
	{
		if (i != 9)
		{
            threads.emplace_back(createPixels, first, tenthSize, i);
		}
        else
        {
            threads.emplace_back(createPixels, first, final, i);
        }
	}

    for (size_t i = 0; i < 10; i++)
    {
        threads[i].join();
    }

    threads.clear();

    // Create second image with 10 threads

    for (size_t i = 0; i < 10; i++)
    {
        if (i != 9)
        {
            threads.emplace_back(createPixels, second, tenthSize, i);
        }
        else
        {
            threads.emplace_back(createPixels, second, final, i);
        }
    }

    for (size_t i = 0; i < 10; i++)
    {
        threads[i].join();
    }

    threads.clear();


	// Create third image with 10 threads
	
	
    for (size_t i = 0; i < 10; i++)
    {
        if (i != 9)
        {
            threads.emplace_back(addPixelColors, first, second, combined, tenthSize, i);
        }
        else
        {
            threads.emplace_back(addPixelColors, first, second, combined, final, i);
        }
    }

    for (size_t i = 0; i < 10; i++)
    {
        threads[i].join();
    }

    chrono::steady_clock::time_point end = chrono::steady_clock::now();

    cout << "Execution time: " << chrono::duration_cast<chrono::milliseconds>(end - begin).count() << "[ms]" << endl;

    delete[] combined;
    delete[] first;
    delete[] second;

}
